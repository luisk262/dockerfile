#! /bin/bash
hdfs --daemon stop namenode
hdfs --daemon stop secondarynamenode
yarn --daemon stop resourcemanager
mapred --daemon stop historyserver
