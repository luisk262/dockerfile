#! /bin/bash
hdfs namenode -format
hdfs --daemon start namenode
hdfs --daemon start secondarynamenode
hdfs dfs -mkdir /tmp
hdfs dfs -chmod g+w /tmp
hdfs dfs -mkdir -p /user/hive/warehouse
hdfs dfs -chmod g+w /user/hive/warehouse
yarn --daemon start resourcemanager
mapred --daemon start historyserver
cd /opt/hive && rm -rf bbdd && mkdir bbdd
cd /opt/hive/bbdd && schematool -dbType derby -initSchema && hiveserver2 &
hdfs dfs -chown hdfs:hdfs /
