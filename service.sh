#! /bin/bash
hdfs --daemon start namenode
hdfs --daemon start secondarynamenode
yarn --daemon start resourcemanager
mapred --daemon start historyserver
cd /opt/hive/bbdd && hiveserver2 &
