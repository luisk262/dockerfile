# Translations template for Hue.
# Copyright (C) 2019 Cloudera, Inc
# This file is distributed under the same license as the Hue project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Hue VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-04-01 11:29-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.1\n"

#: src/hadoop/conf.py:66
msgid "NameNode logical name."
msgstr ""

#: src/hadoop/conf.py:108
msgid "JobTracker logical name."
msgstr ""

#: src/hadoop/conf.py:164
msgid "Resource Manager logical name."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:212
#, python-format
msgid "%(remote_dst)s already exists. Skipping."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:215
#, python-format
msgid "%(remote_dst)s does not exist. Trying to copy."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:225
#, python-format
msgid "Copied %s -> %s."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:227
#, python-format
msgid "Copying %s -> %s failed."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:232
#, python-format
msgid "Skipping %s (not a file)."
msgstr ""

#: src/hadoop/fs/hadoopfs.py:268 src/hadoop/fs/hadoopfs.py:271
#: src/hadoop/fs/hadoopfs.py:274 src/hadoop/fs/hadoopfs.py:277
#: src/hadoop/fs/hadoopfs.py:280 src/hadoop/fs/hadoopfs.py:283
#: src/hadoop/fs/hadoopfs.py:286
#, python-format
msgid "%(function)s has not been implemented."
msgstr ""

#: src/hadoop/fs/upload.py:68
msgid "No HDFS found"
msgstr ""

#: src/hadoop/fs/upload.py:79
#, python-format
msgid "User %s does not have permissions to write to path \"%s\"."
msgstr ""

#: src/hadoop/fs/webhdfs.py:299 src/hadoop/fs/webhdfs.py:336
#, python-format
msgid "File %s not found"
msgstr ""

#: src/hadoop/fs/webhdfs.py:339
#, python-format
msgid "File %s is a directory"
msgstr ""

#: src/hadoop/fs/webhdfs.py:343
#, python-format
msgid "File %s is already trashed"
msgstr ""

#: src/hadoop/fs/webhdfs.py:371
#, python-format
msgid "Delete failed: %s"
msgstr ""

#: src/hadoop/fs/webhdfs.py:400
#, python-format
msgid "File %s is not in trash"
msgstr ""

#: src/hadoop/fs/webhdfs.py:414
#, python-format
msgid "Path %s already exists."
msgstr ""

#: src/hadoop/fs/webhdfs.py:442
#, python-format
msgid "Mkdir failed: %s"
msgstr ""

#: src/hadoop/fs/webhdfs.py:457
#, python-format
msgid "Rename failed: %s -> %s"
msgstr ""

#: src/hadoop/fs/webhdfs.py:463 src/hadoop/fs/webhdfs.py:467
#, python-format
msgid "'%s' is not a directory"
msgstr ""

#: src/hadoop/fs/webhdfs.py:699
#, python-format
msgid "Copy src '%s' does not exist"
msgstr ""

#: src/hadoop/fs/webhdfs.py:701
#, python-format
msgid "Copy src '%s' is a directory"
msgstr ""

#: src/hadoop/fs/webhdfs.py:703
#, python-format
msgid "Copy dst '%s' is a directory"
msgstr ""

#: src/hadoop/fs/webhdfs.py:784
#, python-format
msgid "File not found: %s"
msgstr ""

#: src/hadoop/fs/webhdfs.py:800
#, python-format
msgid "Destination file %s exists and is not a directory."
msgstr ""

#: src/hadoop/fs/webhdfs.py:842
#, python-format
msgid "Failed to create '%s'. HDFS did not return a redirect"
msgstr ""

#: src/hadoop/fs/webhdfs.py:910
msgid "Failed to upload file. WebHdfs requires a valid username to upload files."
msgstr ""

#: src/hadoop/fs/webhdfs.py:941
#, python-format
msgid "Is a directory: '%s'"
msgstr ""

#: src/hadoop/fs/webhdfs.py:959
msgid "Invalid argument to seek for whence"
msgstr ""

#: src/hadoop/fs/webhdfs.py:979
msgid "File not open for writing"
msgstr ""

#: src/hadoop/fs/webhdfs.py:1025
#, python-format
msgid "Filesystem root '/' should be owned by '%s'"
msgstr ""

#: src/hadoop/fs/webhdfs.py:1028
msgid "Failed to access filesystem root"
msgstr ""

#: src/hadoop/fs/webhdfs.py:1036
#, python-format
msgid "Failed to create temporary file \"%s\""
msgstr ""

#: src/hadoop/fs/webhdfs.py:1052
#, python-format
msgid "Failed to remove temporary file \"%s\""
msgstr ""

#: src/hadoop/yarn/history_server_api.py:46
msgid "YARN cluster is not available."
msgstr ""

#: src/hadoop/yarn/mapreduce_api.py:47
#: src/hadoop/yarn/resource_manager_api.py:52
msgid "No Resource Manager are available."
msgstr ""

#: src/hadoop/yarn/resource_manager_api.py:146
msgid "YARN did not return any token field."
msgstr ""

#: src/hadoop/yarn/resource_manager_api.py:173
#, python-format
msgid "YARN RM returned a failed response: %s"
msgstr ""

#: src/hadoop/yarn/spark_history_server_api.py:53
msgid "No Spark History Server is available."
msgstr ""

