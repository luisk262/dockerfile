��          t               �   I   �   C         [  &   |     �  !   �     �  6   �  @   *  1   k  �  �  I   '  C   q      �  &   �     �  !        8  6   M  @   �  1   �   Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:32-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. 