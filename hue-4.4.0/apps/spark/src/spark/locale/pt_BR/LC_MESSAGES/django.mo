��          t               �   I   �   C         [  &   |     �  !   �     �  6   �  @   *  1   k  �  �  I   C  C   �      �  &   �       !   2     T  6   i  @   �  1   �   Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. Project-Id-Version: VERSÃO DO PROJETO
Report-Msgid-Bugs-To: ENDEREÇO DE E-MAIL
POT-Creation-Date: 2019-04-01 11:32-0700
PO-Revision-Date: 2012-07-30 18:50-0700
Last-Translator: NOME COMPLETO <ENDEREÇO DE E-MAIL>
Language: pt_BR
Language-Team: pt_BR <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. 