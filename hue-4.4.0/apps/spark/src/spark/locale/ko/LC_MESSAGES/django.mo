ή          t               Μ   I   Ν   C         [  &   |     £  !   Ό     ή  6   σ  @   *  1   k      I      [   j  )   Ζ  4   π     %  0   E     v  R     b   ή  1   A   Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:32-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ko
Language-Team: ko <LL@li.org>
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 Choose whether Hue should validate certificates received from the server. λ‘μ»¬ 'process' λͺ¨λ λλ 'yarn' μμμλ‘ μμνλλ‘ Livyλ₯Ό κ΅¬μ±ν©λλ€. Livy μλ²μ νΈμ€νΈ μ£Όμμλλ€. SparkSQL μλ²κ° μ€ν μ€μΈ νΈμ€νΈμλλ€. Livy μλ²μ ν¬νΈμλλ€. SparkSQL μλ²κ° μ€νλλ ν¬νΈμλλ€. The Livy Server URL. Livy Spark μλ²λ₯Ό μ€ννμ§ μμΌλ©΄ μ΄ μ±μ μ¬μ©ν  μ μμ΅λλ€. Livyμμ ν΄λΌμ΄μΈνΈμ Kerberos μΈμ¦ μνμ μκ΅¬νλμ§ μ¬λΆλ₯Ό λνλλλ€. Whether Livy requres client to have CSRF enabled. 