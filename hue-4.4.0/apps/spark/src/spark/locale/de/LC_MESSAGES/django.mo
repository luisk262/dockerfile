��          t               �   I   �   C         [  &   |     �  !   �     �  6   �  @   *  1   k  �  �  I   '  i   q     �  7   �     1  7   H     �  A   �  U   �  1   -   Choose whether Hue should validate certificates received from the server. Configure livy to start in local 'process' mode, or 'yarn' workers. Host address of the Livy Server. Host where SparkSQL server is running. Port of the Livy Server. Port the SparkSQL server runs on. The Livy Server URL. The app won't work without a running Livy Spark Server Whether Livy requires client to perform Kerberos authentication. Whether Livy requres client to have CSRF enabled. Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:32-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 Choose whether Hue should validate certificates received from the server. Konfigurieren Sie Livy so, dass das Starten im lokalen "process"-Modus oder mit "yarn"-Arbeitern erfolgt. Hostadresse des Livy-Servers. Der Host, auf dem der SparkSQL-Server ausgeführt wird. Port des Livy-Servers. Der Port, auf dem der SparkSQL-Server ausgeführt wird. The Livy Server URL. Die App funktioniert nicht, wenn kein Livy-Spark-Server aktiv ist Ob Livy für die Durchführung der Kerberos-Authentifizierung einen Client benötigt. Whether Livy requres client to have CSRF enabled. 