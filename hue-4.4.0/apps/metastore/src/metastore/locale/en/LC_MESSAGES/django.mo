��    �      �              	  9   	     G	     K	     `	  >   m	     �	     �	     �	     �	     �	     
     
  '   %
  -   M
     {
     �
  	   �
     �
     �
     �
  
   �
     �
     �
               =  
   Y     d     m  	   �     �     �     �  -   �  7   �  .   '  *   V  1   �  $   �     �  
   �  	   �     �               '     5     Q  	   j     t  )   }  !   �  #   �  1   �  1     .   Q  *   �     �     �  �   �  ;   B     ~     �     �  [   �     �                    $     7     @  	   H     R     d     s     x     |     �     �     �     �     �  '   �     �  Z        p     y  
   �     �  
   �     �  
   �     �  
   �  
   �  
   �     �     �     �          ,     4     I     [     i     n     u     |     �  	   �     �     �     �     �  %   �     �     �  0      )   1  $   [  #   �     �  
   �     �     �     �     �     �     �  C   �     <     @  ,   U  .   �  (   �     �     �     �     �     �     �  �    9   �     �     �     �  >   �     +     2     >     U     o     �     �  '   �  -   �     �       	   "     ,     2     R  
   Z     e     t     �     �     �  
   �     �     �  	             *     2  -   @  7   n  .   �  *   �  1      $   2     W  
   \  	   g     q     �     �     �     �     �  	   �     �  )   �  !   &  #   H  1   l  1   �  .   �  *   �     *     0  �   7  ;   �     �     
       [        y     �     �     �     �     �     �  	   �     �     �     �     �     �     	               +     ?  '   S     {  Z   �     �     �  
          
   !     ,  
   8     C  
   H  
   S  
   ^     i     o     }     �     �     �     �     �     �     �     �     �        	   "      ,      1      7      >   %   D      j      x   0      )   �   $   �   #   �      #!  
   1!     <!     A!     O!     b!     i!     n!  C   w!     �!     �!  ,   �!  .   "  (   0"     Y"     ]"     k"     n"     w"     z"   %(column_name)s (partition key with type %(column_type)s) Add Add a description... Add a filter Alter database requires a properties value of key-value pairs. Browse Browse Data Browse partition files Browse the selected table Can't load the data:  Cancel Cannot browse partition Cannot get metadata for database %s: %s Cannot get queries related to table %s.%s: %s Cannot read partition Cannot read table Character Close Column `%s`.`%s` `%s` not found Columns Complexity Confirm action Create a new database Create a new table Create a new table from a file Create a new table manually Created by DB Error Data last updated on Databases Delete the selected partitions Details Did you know? Do you really want to delete the database(s)? Do you really want to delete the following database(s)? Do you really want to delete the partition(s)? Do you really want to delete the table(s)? Do you really want to drop the selected table(s)? Do you really want to drop the table Drop Drop Table Drop View Drop database %s Drop partition %s Drop partition(s) Drop table %s Drop the selected databases Drop the selected tables Edit path External Failed to alter column `%s`.`%s` `%s`: %s Failed to alter database `%s`: %s Failed to alter table `%s`.`%s`: %s Failed to remove %(databases)s.  Error: %(error)s Failed to remove %(partition)s.  Error: %(error)s Failed to remove %(tables)s.  Error: %(error)s Failed to retrieve tables for database: %s Files Filter Flag to force all metadata calls (e.g. list tables, table or column details...) to happen via HiveServer2 if available instead of Impala. Flag to turn on the new version of the create table wizard. Foreign keys Hive Compatible Id If the sample contains a large number of columns, click a row to select a column to jump to Impala Compatible Import Import Data Kudu Load data in %s.%s Location Managed Metastore Metastore Manager Metastore icon Name New New from file New manually No No data available No matching records No namespaces found No queries found for the current table. No related tables found. Note that loading data will move data from its location into the table's storage location. Overview Overwrite existing data Overwrite? Owner Parameters Partitioned Partitions Path Popularity Privileges Properties Query Query Results Query partition data Query the selected table Refresh Refresh the database Refresh the table Relationships Rows Sample Schema Schema last modified on Skip the trash Sort Desc Spec Stats Submit Table Table '%(table)s' is not partitioned. Table Browser Tables The column stats for this table are not accurate The partition does not contain any values The table does not contain any data. The view does not contain any data. Toggle Assist Total size Type Unsaved Query Value to filter... Values View View SQL Warning: This will drop all tables and objects within the database. Yes Yes, drop this table You are not allowed to modify the metastore. You have must have metastore:write permissions alter_column requires a column parameter and and stored in by location on others Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:31-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 %(column_name)s (partition key with type %(column_type)s) Add Add a description... Add a filter Alter database requires a properties value of key-value pairs. Browse Browse Data Browse partition files Browse the selected table Can't load the data:  Cancel Cannot browse partition Cannot get metadata for database %s: %s Cannot get queries related to table %s.%s: %s Cannot read partition Cannot read table Character Close Column `%s`.`%s` `%s` not found Columns Complexity Confirm action Create a new database Create a new table Create a new table from a file Create a new table manually Created by DB Error Data last updated on Databases Delete the selected partitions Details Did you know? Do you really want to delete the database(s)? Do you really want to delete the following database(s)? Do you really want to delete the partition(s)? Do you really want to delete the table(s)? Do you really want to drop the selected table(s)? Do you really want to drop the table Drop Drop Table Drop View Drop database %s Drop partition %s Drop partition(s) Drop table %s Drop the selected databases Drop the selected tables Edit path External Failed to alter column `%s`.`%s` `%s`: %s Failed to alter database `%s`: %s Failed to alter table `%s`.`%s`: %s Failed to remove %(databases)s.  Error: %(error)s Failed to remove %(partition)s.  Error: %(error)s Failed to remove %(tables)s.  Error: %(error)s Failed to retrieve tables for database: %s Files Filter Flag to force all metadata calls (e.g. list tables, table or column details...) to happen via HiveServer2 if available instead of Impala. Flag to turn on the new version of the create table wizard. Foreign keys Hive Compatible Id If the sample contains a large number of columns, click a row to select a column to jump to Impala Compatible Import Import Data Kudu Load data in %s.%s Location Managed Metastore Metastore Manager Metastore icon Name New New from file New manually No No data available No matching records No namespaces found No queries found for the current table. No related tables found. Note that loading data will move data from its location into the table's storage location. Overview Overwrite existing data Overwrite? Owner Parameters Partitioned Partitions Path Popularity Privileges Properties Query Query Results Query partition data Query the selected table Refresh Refresh the database Refresh the table Relationships Rows Sample Schema Schema last modified on Skip the trash Sort Desc Spec Stats Submit Table Table '%(table)s' is not partitioned. Table Browser Tables The column stats for this table are not accurate The partition does not contain any values The table does not contain any data. The view does not contain any data. Toggle Assist Total size Type Unsaved Query Value to filter... Values View View SQL Warning: This will drop all tables and objects within the database. Yes Yes, drop this table You are not allowed to modify the metastore. You have must have metastore:write permissions alter_column requires a column parameter and and stored in by location on others 