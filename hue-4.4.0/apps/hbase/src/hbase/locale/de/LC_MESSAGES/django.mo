��    _                                    :  	   V     `     t     �     �     �     �     �  *   �          5     A     H  
   V  
   a  I   l     �  :   �  3   �     +     ;     V     b  �   o     ;	     C	     R	     d	     u	  
   �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	     �	     �	  	   �	     
     
     &
     .
     :
  -   H
  ?   v
  
   �
  @   �
                         8  	   @     J     ^     j     v     �     �     �     �     �     �     �     �       �        �     �  
   �     �     �     �     �  
   �  R        T     m     y     �     �     �     �  v   �           (     6     B  �  R     �  /   �  '        G     X     v  "   �     �     �     �  #   �  *     0   ;     l  	   z     �  
   �  
   �  I   �  
   �  >   �  6   =     t     �     �     �  	  �     �     �     �     �          $     3     C     P     X     h     w  
   �  
   �     �     �     �     �     �  	               9   +  G   e     �  P   �  
             +  &   4  
   [     f     s     �     �     �     �     �     �             	   5     ?     Y    t  �   �          !     8     I     b     o     |     �  L   �  %   �       "   !  	   D     N  
   [     f  v   i     �  
   �     �     �   .CSV, .TSV, etc... ? (This cannot be undone) A POST request is required. Add Field Add New Column/Cell Add a column property Add an additional column family Additional column property All Api Error: %s Are you sure you want to Are you sure you want to drop this column? Autocomplete Suggestions: Bulk Upload Cancel Cell History: Cell Value Cell image Choose whether Hue should validate certificates received from the server. Close Cluster by the name of %s does not exist in configuration. Cluster configuration %s isn't formatted correctly. Column Families Column Family: Column Name Column Name Column Range Comma-separated list of HBase Thrift servers for clusters in the format of '(name|host:port)'. Use full hostname with security.Prefix hostname with https:// if using SSL and http mode with impersonation. Confirm Confirm Delete Create New Column Create New Table Current Version Delete Row Delete row  Disable Drop Drop Columns Drop Rows Edit Cell Edited Enable End Column/Family End FilterString End Query End Select Columns Entries after Fetched Full Editor HBase Browser HBase Thrift 1 server cannot be contacted: %s HBase configuration directory, where hbase-site.xml is located. HBase icon Hard limit of rows or columns per row fetched before truncating. Home Insert New Row Integer Length of Scan or Row Key New Row New Table No rows to display. Prefix Scan Refresh Row Remove Column Family Remove column property Revert Row Key Row Key Value Rows starting with Save Search for Table Name Select All Visible Should come from hbase-site.xml, do not set. 'framed' is used to chunk up responses, used with the nonblocking server in Thrift but is not supported in Hue.'buffered' used to be the default of the HBase Thrift Server. Default is buffered when not set in hbase-site.xml. Should come from hbase-site.xml, do not set. Force Hue to use Http Thrift mode with doas impersonation, regarless of hbase-site.xml properties. Sort By Start FilterString Start Scan Start Select Columns String Submit Switch Cluster Table Name The kerberos principal name is missing from the hbase-site.xml configuration file. Toggle Collapse Selected Toggle Grid Toggle Select All Rows Upload disable enable in row_key, row_prefix* +scan_len [col1, family:col2, fam3:, col_prefix* +3, fam: col2 to col3] {Filter1() AND Filter2()} seconds starting from this table? were truncated. Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:31-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 .CSV, .TSV, usw... ? (Dies kann nicht rückgängig gemacht werden) Eine POST-Anforderung ist erforderlich. Feld hinzufügen Neue Spalte/Zelle hinzufügen Spalteneigenschaft hinzufügen Weitere Spaltenfamilie hinzufügen Zusätzliche Spalteneigenschaft Alle API-Fehler: %s Möchten Sie diese Tabelle wirklich Soll diese Spalte wirklich beendet werden? Vorschläge zum automatischen Vervollständigen: Massen-Upload Abbrechen Zellenverlauf: Zellenwert Zellenbild Choose whether Hue should validate certificates received from the server. Schließen Cluster mit Namen %s ist in der Konfiguration nicht vorhanden. Cluster-Konfiguration %s ist nicht korrekt formatiert. Spaltenfamilien Spaltenfamilie: Spaltenname Spaltenname Spaltenbereich Durch Kommas abgetrennte Liste von HBase Thrift-Servern für Cluster im Format "(name|host:port)". Verwenden Sie den vollständigen Hostnamen mit Sicherheit. Stellen Sie dem Hostnamen https:// voran, wenn Sie SSL und den HTTP-Modus mit Identitätswechsel verwenden. Bestätigen Löschen bestätigen Neue Spalte erstellen Neue Tabelle erstellen Aktuelle Version Zeile löschen Zeile löschen  Deaktivieren Ablegen Spalten ablegen Zeilen ablegen Zelle bearbeiten Bearbeitet Aktivieren Ende der Spalte/Familie Ende von FilterString Abfrage beenden Ende der Spaltenauswahl Einträge nach Abgerufen Gesamter Editor HBase-Browser HBase Thrift 1-Server konnte nicht kontaktiert werden: %s Verzeichnis der Hive-Konfiguration, in dem sich hive-site.xml befindet. HBase-Symbol Feste Begrenzung der Zeilen oder Spalten pro vor der Kürzung abgerufener Zeile. Startseite Neue Zeile einfügen Ganzzahl Länge des Scans oder Zeilenschlüssel Neue Zeile Neue Tabelle Keine anzuzeigenden Zeilen. Präfix-Scan Zeile aktualisieren Spaltenfamilie entfernen Spalteneigenschaft entfernen Rückgängig machen Zeilenschlüssel Zeilenschlüsselwert Zeilen beginnend mit Speichern Nach Tabellennamen suchen Alle sichtbaren auswählen Should come from hbase-site.xml, do not set. 'framed' is used to chunk up responses, used with the nonblocking server in Thrift but is not supported in Hue.'buffered' used to be the default of the HBase Thrift Server. Default is buffered when not set in hbase-site.xml. Should come from hbase-site.xml, do not set. Force Hue to use Http Thrift mode with doas impersonation, regarless of hbase-site.xml properties. Sortieren nach Start von FilterString Scannen beginnen Start der Spaltenauswahl Zeichenfolge Übermitteln Cluster umschalten Tabellenname Der Kerberos-Principal-Name fehlt in der Konfigurationsdatei hbase-site.xml. Umschalten von Ausblenden ausgewählt Raster umschalten Auswählen aller Zeilen umschalten Hochladen deaktivieren aktivieren in row_key, row_prefix* +scan_len [col1, family:col2, fam3:, col_prefix* +3, fam: col2 to col3] {Filter1() AND Filter2()} Sekunden beginnt am ? wurden verkürzt. 