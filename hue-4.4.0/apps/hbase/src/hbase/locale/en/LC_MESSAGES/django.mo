��    _                                    :  	   V     `     t     �     �     �     �     �  *   �          5     A     H  
   V  
   a  I   l     �  :   �  3   �     +     ;     V     b  �   o     ;	     C	     R	     d	     u	  
   �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	     �	     �	  	   �	     
     
     &
     .
     :
  -   H
  ?   v
  
   �
  @   �
                         8  	   @     J     ^     j     v     �     �     �     �     �     �     �     �       �        �     �  
   �     �     �     �     �  
   �  R        T     m     y     �     �     �     �  v   �           (     6     B  �  R     �     �     	  	   %     /     C     Y     y     �     �     �  *   �     �                 
   %  
   0  I   ;     �  :   �  3   �     �     
     %     1  �   >     
          !     3     D  
   T     _     k     s     x  	   �  	   �     �     �     �     �  	   �     �     �     �     �     	  -     ?   E  
   �  @   �     �     �     �     �       	             -     9     E     Z     q     x     �     �     �     �     �    �  �   �     m     u  
   �     �     �     �     �  
   �  R   �     #     <     H     _     f     n     u  v   x     �     �             .CSV, .TSV, etc... ? (This cannot be undone) A POST request is required. Add Field Add New Column/Cell Add a column property Add an additional column family Additional column property All Api Error: %s Are you sure you want to Are you sure you want to drop this column? Autocomplete Suggestions: Bulk Upload Cancel Cell History: Cell Value Cell image Choose whether Hue should validate certificates received from the server. Close Cluster by the name of %s does not exist in configuration. Cluster configuration %s isn't formatted correctly. Column Families Column Family: Column Name Column Name Column Range Comma-separated list of HBase Thrift servers for clusters in the format of '(name|host:port)'. Use full hostname with security.Prefix hostname with https:// if using SSL and http mode with impersonation. Confirm Confirm Delete Create New Column Create New Table Current Version Delete Row Delete row  Disable Drop Drop Columns Drop Rows Edit Cell Edited Enable End Column/Family End FilterString End Query End Select Columns Entries after Fetched Full Editor HBase Browser HBase Thrift 1 server cannot be contacted: %s HBase configuration directory, where hbase-site.xml is located. HBase icon Hard limit of rows or columns per row fetched before truncating. Home Insert New Row Integer Length of Scan or Row Key New Row New Table No rows to display. Prefix Scan Refresh Row Remove Column Family Remove column property Revert Row Key Row Key Value Rows starting with Save Search for Table Name Select All Visible Should come from hbase-site.xml, do not set. 'framed' is used to chunk up responses, used with the nonblocking server in Thrift but is not supported in Hue.'buffered' used to be the default of the HBase Thrift Server. Default is buffered when not set in hbase-site.xml. Should come from hbase-site.xml, do not set. Force Hue to use Http Thrift mode with doas impersonation, regarless of hbase-site.xml properties. Sort By Start FilterString Start Scan Start Select Columns String Submit Switch Cluster Table Name The kerberos principal name is missing from the hbase-site.xml configuration file. Toggle Collapse Selected Toggle Grid Toggle Select All Rows Upload disable enable in row_key, row_prefix* +scan_len [col1, family:col2, fam3:, col_prefix* +3, fam: col2 to col3] {Filter1() AND Filter2()} seconds starting from this table? were truncated. Project-Id-Version: Hue VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-04-01 11:31-0700
PO-Revision-Date: 2013-10-28 10:31-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: en
Language-Team: en <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.1
 .CSV, .TSV, etc... ? (This cannot be undone) A POST request is required. Add Field Add New Column/Cell Add a column property Add an additional column family Additional column property All Api Error: %s Are you sure you want to Are you sure you want to drop this column? Autocomplete Suggestions: Bulk Upload Cancel Cell History: Cell Value Cell image Choose whether Hue should validate certificates received from the server. Close Cluster by the name of %s does not exist in configuration. Cluster configuration %s isn't formatted correctly. Column Families Column Family: Column Name Column Name Column Range Comma-separated list of HBase Thrift servers for clusters in the format of '(name|host:port)'. Use full hostname with security.Prefix hostname with https:// if using SSL and http mode with impersonation. Confirm Confirm Delete Create New Column Create New Table Current Version Delete Row Delete row  Disable Drop Drop Columns Drop Rows Edit Cell Edited Enable End Column/Family End FilterString End Query End Select Columns Entries after Fetched Full Editor HBase Browser HBase Thrift 1 server cannot be contacted: %s HBase configuration directory, where hbase-site.xml is located. HBase icon Hard limit of rows or columns per row fetched before truncating. Home Insert New Row Integer Length of Scan or Row Key New Row New Table No rows to display. Prefix Scan Refresh Row Remove Column Family Remove column property Revert Row Key Row Key Value Rows starting with Save Search for Table Name Select All Visible Should come from hbase-site.xml, do not set. 'framed' is used to chunk up responses, used with the nonblocking server in Thrift but is not supported in Hue.'buffered' used to be the default of the HBase Thrift Server. Default is buffered when not set in hbase-site.xml. Should come from hbase-site.xml, do not set. Force Hue to use Http Thrift mode with doas impersonation, regarless of hbase-site.xml properties. Sort By Start FilterString Start Scan Start Select Columns String Submit Switch Cluster Table Name The kerberos principal name is missing from the hbase-site.xml configuration file. Toggle Collapse Selected Toggle Grid Toggle Select All Rows Upload disable enable in row_key, row_prefix* +scan_len [col1, family:col2, fam3:, col_prefix* +3, fam: col2 to col3] {Filter1() AND Filter2()} seconds starting from this table? were truncated. 